import Button from './src/components/button';
import Input from './src/components/input';
import Colors from './src/constants/colors';
import Size from './src/constants/size';
import Default from './src/constants/default';
import { Atoms } from './src/core';

import Switch from './src/components/switch/switch';

const Text = Atoms.Text;
// const Switch = Atoms.Switch;

export {
  Button,
  Input,
  Colors,
  Size,
  Default,
  Text,
  Switch,
}
