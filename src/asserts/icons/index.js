import WebCameraON from './webcamera/webcamera-on.svg';
import WebCameraOFF from './webcamera/webcamera-off.svg';

import AlertON from './alert/alert-on.svg';
import AlertOFF from './alert/alert-off.svg';

import SensorsON from './sensors/sensors-on.svg';
import SensorsOFF from './sensors/sensors-off.svg';

import WifiON from './wifi/wifi-on.svg';
import WifiOFF from './wifi/wifi-off.svg';

import LightON from './light/light-on.svg';
import LightOFF from './light/light-off.svg';

import MoneyON from './money/money-on.svg';
import MoneyOFF from './money/money-off.svg';

import DevicesON from './devices/devices-on.svg';
import DevicesOFF from './devices/devices-off.svg';

import OutletON from './outlet/outlet-on.svg';
import OutletOFF from './outlet/outlet-off.svg';

import WaterON from './water/water-on.svg';
import WaterOFF from './water/water-off.svg';

import TemperatureON from './temperature/temperature-on.svg';
import TemperatureOFF from './temperature/temperature-off.svg';

import SpeakerON from './speaker/speaker-on.svg';
import SpeakerOFF from './speaker/speaker-off.svg';

import SecurityON from './security/security-on.svg'
import SecurityOFF from './security/security-off.svg'

export default {
  webcamera: {
    on: WebCameraON,
    off: WebCameraOFF
  },

  alert: {
    on: AlertON,
    off: AlertOFF
  },

  sensors: {
    on: SensorsON,
    off: SensorsOFF
  },

  wifi: {
    on: WifiON,
    off: WifiOFF
  },

  light: {
    on: LightON,
    off: LightOFF
  },

  money: {
    on: MoneyON,
    off: MoneyOFF
  },

  devices: {
    on: DevicesON,
    off: DevicesOFF
  },

  outlet: {
    on: OutletON,
    off: OutletOFF
  },

  water: {
    on: WaterON,
    off: WaterOFF
  },

  temperature: {
    on: TemperatureON,
    off: TemperatureOFF
  },

  speaker: {
    on: SpeakerON,
    off: SpeakerOFF
  },

  security: {
    on: SecurityON,
    off: SecurityOFF
  }
};
