export default {
  color: {
    blue: '#1F8EFA',
    orange: '#FFAB4F',
    red: '#EE423D',
    green: '#05C985',
    secondary: '#2F3B52',
    primary: '#242E42',
    grey: '#979797',
    white: '#FFFFFF',
    neutral: '#3E4E6C',
    secondary2: '#70889E',
    circle: {
      disabled: '#70889E',
      enabled: '#1F8EFA'
    }
  },
};
