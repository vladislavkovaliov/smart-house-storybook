export default {
  black: '#000000',
  blue: '#1F8EFA',
  orange: '#FFAB4F',
  red: '#EE423D',
  green: '#05C985',
  primary: '#242E42',
  grey: '#979797',
  white: '#FFFFFF',
  secondary: '#2F3B52',
  secondary2: '#3E4E6C',
  secondary3: '#70889E',
};