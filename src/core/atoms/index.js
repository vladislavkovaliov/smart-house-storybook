import Text from './text/text';
import Switch from './switch/switch';

export {
  Text,
  Switch,
}

export default {
  Text,
  Switch,
}
