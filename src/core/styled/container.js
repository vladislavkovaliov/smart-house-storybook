import { width } from 'styled-system';
import styled from 'styled-components';

export default styled.div`
  ${width};
`;
