import styled from 'styled-components';
import {width, flex} from "styled-system";

export default styled.div`
  display: flex;
  ${flex}
`;
