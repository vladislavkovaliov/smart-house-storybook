import styled from 'styled-components';
import { flex, layout, space } from 'styled-system';

const FlexBox = styled.div`
  display: flex;
  ${flex};
  ${layout};
  ${space};
`;

export default FlexBox;