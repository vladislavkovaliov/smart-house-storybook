import styled from 'styled-components';
import colors from '../../../constants/colors';
import Default from '../../../constants/default';

export default styled.div`
  color: ${colors.white};
  font-size: ${Default.fontSize + 4}px;
`;
