import styled from 'styled-components';

export default styled.div`
  flex-grow: 1;

  div {
    margin-bottom: 4px;
  }
`;
