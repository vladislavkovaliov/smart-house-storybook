import styled from 'styled-components';
import colors from '../../../constants/colors';

export default styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  max-width: 218px;
  min-width: 218px;
  height: 99px;
  border-radius: 5px;
  background-color: ${colors.secondary2};
`;
