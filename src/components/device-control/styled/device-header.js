import styled from 'styled-components';

export default styled.div`
  margin: 8px 8px 4px 8px;
  display: flex;
  justify-content: flex-end;
`;
