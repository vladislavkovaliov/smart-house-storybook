import styled from 'styled-components';

export default styled.div`
  display: flex;
  padding: 4px;
  flex-grow: 1;
  flex-direction: row;
`;
