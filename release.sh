#!/usr/bin/env bash

PAYLOAD=$(cat <<- JSON
{
  "tag_name": "$CI_COMMIT_TAG",
  "name": "$CI_PROJECT_NAME",
  "description": "$CI_COMMIT_TAG_MESSAGE\\nNo release notes",
  "assets": {
    "links": [
      {
        "name": "$CI_PROJECT_NAME $CI_COMMIT_TAG",
        "url": "$CI_API_V4_URL/projects/${CI_PROJECT_ID}/jobs/artifacts/$CI_COMMIT_TAG/download?job=$CI_JOB_NAME"
      }
    ]
  }
}
JSON
)

echo "$PAYLOAD"

curl \
    --header "Content-Type: application/json" \
    --header "PRIVATE-TOKEN: ${CI_PRIVATE_TOKEN}" \
    --data "$PAYLOAD" \
    --request POST \
      "$CI_API_V4_URL/projects/${CI_PROJECT_ID}/releases"

echo Stop