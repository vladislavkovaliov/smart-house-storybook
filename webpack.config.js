var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: './index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
    library: '',
    libraryTarget: 'commonjs'
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
        },
      }
    ]
  },
  devtool: 'source-map',
  plugins: [
    new webpack.SourceMapDevToolPlugin({})
  ],
  externals: {
    react: 'react', // Case matters here
    'react-dom': 'react-dom' // Case matters here
  }
};